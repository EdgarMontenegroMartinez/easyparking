package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O17Application {

	public static void main(String[] args) {
		SpringApplication.run(O17Application.class, args);
	}

}
