package com.Services;
import com.TO.Personas;
import java.util.List;

public interface IPersonasService {
    public Personas guardar(Personas persona);

    public void eliminar(int idpersonas);

    public List<Personas> listarpersonas();

    public Personas buscarPersona(int idPersona);
}
