package com.Ctrl;

//import com.Services.PersonasService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.Services.UsuariosService;
import com.TO.Usuarios;

@Controller
public class UsuariosCtrl {

    @Autowired
    private UsuariosService usuariosService;

    //@Autowired
   // private PersonasService personasService;

    /*METODOS PARA POSTMAN*/
    @PostMapping("/usuarios/registrar")
    public ResponseEntity<Usuarios> registrar(@RequestBody Usuarios usuario) {
        return new ResponseEntity<>(usuariosService.guardar(usuario), HttpStatus.OK);
    }

    @GetMapping("/usuarios/listar")
    public ResponseEntity<List<Usuarios>> listarUsuarios() {
        return new ResponseEntity<>(usuariosService.listarusuarios(), HttpStatus.OK);
    }
    
    @RequestMapping(value="/usuarios/eliminar/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<String> eliminar(@PathVariable int id) {
        Usuarios usr = usuariosService.buscarUsuario(id);
        if (usr != null) {
            usuariosService.eliminar(id);
            return new ResponseEntity<>("Registro eliminado", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Registro no encontrado", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /*METODOS PARA LA PAGINA HTML*/
    
    @GetMapping("/usuarios")
    public String inicio(Model model) {
        model.addAttribute("titulo", "Usuarios");
        Usuarios usuario = new Usuarios();
        model.addAttribute("usuario", usuario);
        //var personas = personasService.listarPersonas();
        //model.addAttribute("personas", personas);
        List<Usuarios> usuarios = usuariosService.listarusuarios();
        model.addAttribute("usuarios", usuarios);
        return "Usuarios";
    }
    
    @PostMapping("/usuarios/guardar")
    public String guardar(Usuarios usuario) {
        usuariosService.guardar(usuario);
        return "redirect:/usuarios";
    }

}
