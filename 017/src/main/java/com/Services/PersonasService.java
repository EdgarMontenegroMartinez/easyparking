package com.Services;

import com.DAO.PersonasDAO;
import com.TO.Personas;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;




@Service
public class PersonasService implements IPersonasService{

    @Autowired
    PersonasDAO usuarioDao;

    @Override
    @Transactional
    public Personas guardar(Personas usuario) {
        return usuarioDao.save(usuario);
    }

    @Override
    @Transactional
    public void eliminar(int idPersona) {
        usuarioDao.deleteById(idPersona);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Personas> listarpersonas() {
        return (List<Personas>) usuarioDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Personas buscarPersona(int idPersona) {
        return usuarioDao.findById(idPersona).orElse(null);
    }
    
}
