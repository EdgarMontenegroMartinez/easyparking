package com.Services;
import com.TO.Usuarios;
import java.util.List;

public interface IUsuariosService {
    public Usuarios guardar(Usuarios usuario);

    public void eliminar(int idusuarios);

    public List<Usuarios> listarusuarios();

    public Usuarios buscarUsuario(int idUsuario);
}
