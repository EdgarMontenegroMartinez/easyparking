package com.Ctrl;

//import com.Services.PersonasService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.Services.PersonasService;
import com.TO.Personas;

@Controller
public class PersonasCtrl {

    @Autowired
    private PersonasService personasService;

    //@Autowired
   // private PersonasService personasService;

    /*METODOS PARA POSTMAN*/
    @PostMapping("/personas/registrar")
    public ResponseEntity<Personas> registrar(@RequestBody Personas persona) {
        return new ResponseEntity<>(personasService.guardar(persona), HttpStatus.OK);
    }

    @GetMapping("/personas/listar")
    public ResponseEntity<List<Personas>> listarPersonas() {
        return new ResponseEntity<>(personasService.listarpersonas(), HttpStatus.OK);
    }
    
    @CrossOrigin()
    @RequestMapping(value="/personas/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<String> eliminar(@PathVariable int id) {
        Personas usr = personasService.buscarPersona(id);
        if (usr != null) {
            personasService.eliminar(id);
            return new ResponseEntity<>("Registro eliminado", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Registro no encontrado", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /*METODOS PARA LA PAGINA HTML*/
    @CrossOrigin()
    @GetMapping("/personas")
    public ResponseEntity<List<Personas>> inicio() {
        
        return new ResponseEntity<>(personasService.listarpersonas(), HttpStatus.OK) ;

    }
    
    @CrossOrigin()
    @PostMapping("/personas")
    public ResponseEntity<Personas> crear(@RequestBody Personas persona) {
        return new ResponseEntity<>(personasService.guardar(persona), HttpStatus.CREATED) ;
    }
    
    @CrossOrigin()
    @PutMapping("/personas")
    public ResponseEntity<Personas> actualizar(@RequestBody Personas persona) {
        return new ResponseEntity<>(personasService.guardar(persona), HttpStatus.CREATED) ;
    }

}
